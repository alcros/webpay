<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @link       http://oraqus.cl
 * @since      1.0.0
 *
 * @package    Oraqus WC Transbank
 */
// If uninstall not called from WordPress, then exit.
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}
