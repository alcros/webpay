=== Oraqus WooCommerce Webpay ===
Contributors: Oraqus
Donate link: http://oraqus.cl/
Tags: woocommerce, payments
Requires at least: 4.6
Tested up to: 4.7.2
License: EULA
License URI: https://oraqus.cl/licenses/eula.html

Plataforma de pagos para WooCommerce a través de Transbank

== Description ==

Este plugin provee a WooCommerce una pasarela de pagos para Transbank,
las pasarelas de pago vienen con algunas variaciones:

Form based – This is where the user must click a button on a form that then redirects them to the payment processor on the gateway’s own website. Example: PayPal standard, Authorize.net DPM
iFrame based – This is when the gateway payment system is loaded inside an iframe on your store. Example: SagePay Form, PayPal Advanced
Direct – This is when the payment fields are shown directly on the checkout page and the payment is made when ‘place order’ is pressed. Example: PayPal Pro, Authorize.net AIM
Offline – No online payment is made. Example: Cheque, Bank Transfer
A longer description for the plugin that contains all the dea


== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Unzip the plugin
2. Upload the plugin directory to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
3. Activate the plugin through the 'Plugins' screen in WordPress
4. Use the Settings->Plugin Name screen to configure the plugin


== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

== Changelog ==

= 0.1 =
First version of the plugin.

