<?php
if (!defined('ABSPATH')) {
    exit;
}

?>

<header>
    <h2><?php _e('Payment Details', 'oraqus-wc-transbank'); ?></h2>
</header>
<!--table class="shop_table order_details"-->
<table class="shop_table">
    <tbody>
            <?php if (isset($voucher->detailOutput->responseDescription)): ?>
            <tr>
                <th><?php _e('Response Description:', 'oraqus-wc-transbank'); ?></th>
                <td><strong><em><?php echo $voucher->detailOutput->responseDescription; ?></em></strong></td>
            </tr>
            <?php endif; ?>

            <?php if ($voucher->detailOutput->authorizationCode and ! isset($voucher->detailOutput->responseDescription)): ?>
            <tr>
                <th><?php _e('Authorization Code:', 'oraqus-wc-transbank'); ?></th>
                <td><?php echo $voucher->detailOutput->authorizationCode; ?></td>
            </tr>
            <?php endif; ?>

            <?php if (isset($voucher->detailOutput->amount)): ?>
            <tr>
                <th><?php _e('Business Name:', 'oraqus-wc-transbank'); ?></th>
                <td><?php echo get_bloginfo('name'); ?></td>
            </tr>
            <?php endif; ?>

            <?php if (isset($voucher->detailOutput->amount)): ?>
            <tr>
                <th><?php _e('Amount:', 'oraqus-wc-transbank'); ?></th>
                <td><?php echo wc_price($voucher->detailOutput->amount); ?></td>
            </tr>
            <?php endif; ?>

            <?php if (isset($voucher->detailOutput->sharesNumber)): ?>
            <tr>
                <th><?php _e('Type of Payments:', 'oraqus-wc-transbank'); ?></th>
                <td><?php echo $type_of_payments; ?></td>
            </tr>
            <?php endif; ?>

            <?php if (isset($voucher->detailOutput->sharesNumber)): ?>
            <tr>
                <th><?php _e('Number of Payments:', 'oraqus-wc-transbank'); ?></th>
                <td><?php echo $voucher->detailOutput->sharesNumber; ?></td>
            </tr>
            <?php endif; ?>

            <?php if ($voucher->transactionDate): ?>
            <tr>
                <th><?php _e('Transaction Date:', 'oraqus-wc-transbank'); ?></th>
                <td><?php echo (new DateTime($voucher->transactionDate))->format(get_option('date_format')); ?></td>
            </tr>
            <?php endif; ?>

            <?php if ($voucher->detailOutput->paymentTypeCode): ?>
            <tr>
                <th><?php _e('Payment Type:', 'oraqus-wc-transbank'); ?></th>
                <td><?php echo $payment_type_description; ?></td>
            </tr>
            <?php endif; ?>

            <?php if ($voucher->cardDetail->cardNumber): ?>
            <tr>
                <th><?php _e('Card Number:', 'oraqus-wc-transbank'); ?></th>
                <td><?php echo $voucher->cardDetail->cardNumber; ?></td>
            </tr>
            <?php endif; ?>

            <?php if ($voucher->cardDetail->cardExpirationDate): ?>
            <tr>
                <th><?php _e('Card Expiration Date:', 'oraqus-wc-transbank'); ?></th>
                <td><?php echo $voucher->cardDetail->cardExpirationDate; ?></td>
            </tr>
            <?php endif; ?>

            <?php if ($voucher->sessionId): ?>
            <tr>
                <th><?php _e('Merchant Session Identifier:', 'oraqus-wc-transbank'); ?></th>
                <td><?php echo $voucher->sessionId; ?></td>
            </tr>
            <?php endif; ?>
    </tbody>
</table>
