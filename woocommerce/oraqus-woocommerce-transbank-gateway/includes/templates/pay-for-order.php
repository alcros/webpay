<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

?>

<div class="form-row">
    <form action="<?php echo $result->url; ?>" method="post">
        <input type="hidden" name="token_ws" value="<?php echo $result->token; ?>"></input>
        <input type="submit" class="button alt" value="<?php _e('Proceed with payment', 'oraqus-wc-transbank'); ?>"></input>
    </form>
    <hr />
    <em>
        <?php _e('Please be aware that this button will redirect you to an external site to guarantee the security of your payment', 'oraqus-wc-transbank'); ?>
    </em>
</div>

<?php

