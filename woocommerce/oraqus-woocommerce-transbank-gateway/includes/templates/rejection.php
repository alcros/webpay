<?php
if (!defined('ABSPATH')) {
    exit;
}

?>

<header>
    <h2><?php _e('Payment Details', 'oraqus-wc-transbank'); ?></h2>
</header>
<hr />
<h4><?php _e('Rejected Transaction', 'oraqus-wc-transbank'); ?></h4>
<h5><?php echo sprintf(__('Purchase Order: %1$s', 'oraqus-wc-transbank'), $order->id); ?></h5>
<span><?php _e('Possible rejection causes:', 'oraqus-wc-transbank'); ?></span>
<ul>
    <li><?php _e('Wrong credit card information (date and/or verification code)', 'oraqus-wc-transbank'); ?></li>
    <li><?php _e('Insufficient funds', 'oraqus-wc-transbank'); ?></li>
    <li><?php _e('Invalid/Disabled credit card', 'oraqus-wc-transbank'); ?></li>
</ul>
<hr />
