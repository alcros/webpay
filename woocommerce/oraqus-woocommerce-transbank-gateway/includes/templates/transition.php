<?php

$bg = home_url(sprintf('/wp-content/plugins/%s/includes/img/transbank-bg.gif', ORAQUS_TBK_PLUGIN_DIR_NAME));

?>

<html lang="en">
    <head>
        <style type="text/css">
            body {
                background-image:url('<?php echo $bg ?>');
            }
        </style>
    </head>
    <body>
        <form id="launcher" action="<?php echo $destination; ?>" method="post">
            <input type="hidden" name="token_ws" value="<?php echo $token; ?>"></input>
        </form>
    </body>
    <script language="javascript">
        document.forms["launcher"].submit();
    </script>
</html>

