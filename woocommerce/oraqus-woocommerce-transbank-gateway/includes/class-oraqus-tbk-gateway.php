<?php
/**
 * Oraqus_Tbk_Gateway
 *
 * @category   Class
 * @package    Oraqus_Tbk_Gateway
 * @subpackage Oraqus_Tbk_Gateway/includes
 * @author     Oraqus
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link       http://oraqus.cl
 */
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Main Class
 */
class Oraqus_Tbk_Gateway extends WC_Payment_Gateway {
    /**
     * Plugin version.
     */
    const VERSION = '1.0';

    /**
     * This plugin's Id.
     */
    const PLUGIN_ID = 'oraqustbk';

    /**
     * transition handler
     */
    const TRANSITION_API_NAME = 'transition_handler';

    /**
     * This plugin's post_meta key to store a unique session id.
     */
    const SESSION_ID_META_KEY = '_session_id';

    /**
     * This plugin's post_meta key to store steps achieved it the flow.
     */
    const FLOW_META_KEY = '_flow';

    /**
     * This plugin's post_meta key to store information about the order.
     */
    const TOKEN_ORDER_PARAMS_META_KEY = '_token_order_params';

    /**
     * This plugin's post_meta key to store information about the payment.
     */
    const PAYMENT_RESULT_META_KEY = '_payment_result';

    /**
     * @var      Oraqus_Tbk_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * @var      Oraqus_Tbk_Credentials $credentials  The credentias used to connect to Transbank.
     */
    protected $credentials;

    /**
     * @var      Oraqus_Tbk_Client $client  The client used to connect to Transbank.
     */
    protected $client;

    /**
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * @var      array $PAYMENT_TYPES    A map of keys to descriptions of payment types.
     */
    private static $PAYMENT_TYPES = array(
        'VD' => 'Debito',
        'VN' => 'Credito',
        'VC' => 'Credito',
        'SI' => 'Credito',
        'S2' => 'Credito',
        'NC' => 'Credito',
    );

    /**
     * @var      array $MULTIPLE_PAYMENT_TYPES    A map of keys to descriptions of multiple payment types.
     */
    private static $MULTIPLE_PAYMENT_TYPES = array(
        'VD' => 'Debito',
        'VN' => 'Sin Cuotas',
        'VC' => 'Cuotas Normales',
        'SI' => 'Sin Interes',
        'S2' => 'Sin Interes',
        'NC' => 'Sin Interes',
    );

    /**
     * Constructor
     */
    public function __construct() {
        $this->plugin_name = 'Oraqus WC Transbank Gateway';
        $this->id = self::PLUGIN_ID;
        $this->has_fields = false;
        $this->method_title = 'Oraqus Transbank';
        $this->method_description = __('Payments through Transbank\'s Webpay system.', 'oraqus-wc-transbank');
        $this->icon = null;

        $this->load_dependencies();

        $this->init_form_fields();
        $this->init_settings();

        $this->extract_settings_to_vars();
        $this->save_settings();

        $this->loader = new Oraqus_Tbk_Loader();
        $this->credentials = Oraqus_Tbk_Credentials::create($this->settings);
        $this->client = new Oraqus_Tbk_Client($this->environment, $this->credentials);

        $this->set_locale();

        $this->define_admin_hooks();
        $this->define_public_hooks();
        $this->run_hook_registration();
    }

    /**
     * Initialize form fields.
     */
    public function init_form_fields() {
        $this->form_fields = include('oraqus-tbk-settings.php');
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     */
    public function run_hook_registration() {
        $this->loader->register();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name() {
        return $this->plugin_name;
    }

    /**
     * Admin Panel Options.
     */
    public function admin_options() {
        if ($this->is_valid_for_use()) {
            parent::admin_options();
        } else {
            echo '<div class="inline error"><p><strong>'
                . _e('Gateway disabled', 'oraqus-wc-transbank')
                . '</strong>: '
                . _e('Oraqus Transbank plugin does not support your store currency.', 'oraqus-wc-transbank')
                . '</p></div>';
        }
    }

    /**
     * Process the payment.
     */
    public function process_payment($order_id) {
        Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s]', __FUNCTION__, 'processing payment for order', $order_id)); 
        $order = wc_get_order($order_id);

        if (isset($response['error'])) {
            wc_add_notice(__('Couldn\'t connect to <em>Webpay</em>', 'oraqus-wc-transbank'), 'error');
            return;
        }

        return array(
            'result' => 'success',
            'redirect' => $order->get_checkout_payment_url(true)
       );
    }

    /**
     * Receipt page.
     *
     * @param  int $order_id
     */
    public function generate_receipt($order_id) {
        $order = wc_get_order($order_id);
        $this->client->set_receipt_url($this->get_return_url($order));

        $response = $this->client->invoke_service($order);

        Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s]', __FUNCTION__, 'response after invoking external service', print_r($response, true))); 
        if (!empty($response['error']) or !empty($response['detail'])) {
            if (!$this->is_flow_complete($order) and $order->has_status('pending')) {
                $err = array();
                $err['error'] = "Incomplete flow";
                $err['detail'] = sprintf('Current flow includes the following steps: %1$s', print_r($flow, true));
                $this->client->update_order_status($order, $err);
                wp_redirect($this->get_return_url($order));
                exit;
            }
            wc_add_notice(__('Couldn\'t connect to <em>Webpay</em>, please try again later.', 'oraqus-wc-transbank'), 'error');
        }

        return;
    }

    /**
     * Payment details.
     */
    public function show_payment_details($order) {
        if (!$order) return;

        $voucher = get_post_meta($order->id, Oraqus_Tbk_Gateway::PAYMENT_RESULT_META_KEY, true);
        if (empty($voucher)) {
            exit;
        }

        $session_id = get_post_meta($order->id, Oraqus_Tbk_Gateway::SESSION_ID_META_KEY, true);
        if ($voucher->sessionId != $session_id) {
            Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [sent=%3$s | recv=%4$s]', __FUNCTION__, 'session id\'s don\'t match', $session_id, $voucher->sessionId)); 
        }

        $type_of_payments = $this->get_multiple_payments_description(isset($voucher->detailOutput)
            ? $voucher->detailOutput->paymentTypeCode
            : __('multiple payments type not defined', 'oraqus-wc-transbank')
        );

        $payment_type_description = $this->get_type_description(isset($voucher->detailOutput)
            ? $voucher->detailOutput->paymentTypeCode
            : __('payment type not defined', 'oraqus-wc-transbank')
        );

        require_once ORAQUS_TBK_PLUGIN_PATH . 'includes/templates/payment-details.php';
    }

    /**
     * show the rejection note, it includes possible clauses for the rejection (required by Transbank).
     */
    public function show_rejection_note($order) {
        if (!$order) return;

        if ($order->has_status('failed')) {
            require_once ORAQUS_TBK_PLUGIN_PATH . 'includes/templates/rejection.php';
        }
    }

    /**
     * Order received process.
     */
    public function order_received_process($order_id) {
        if (!$order_id) return;
        $order = wc_get_order($order_id);

        Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s] - %4$s', __FUNCTION__, 'order status', $order_id, print_r($order->get_status(), true))); 
        if (!$this->is_flow_complete($order) and $order->has_status('pending')) {
            $err = array();
            $err['error'] = "Incomplete flow";
            $err['detail'] = sprintf('Current flow includes the following steps: %1$s', print_r($flow, true));
            $this->client->update_order_status($order, $err);
            wp_redirect($this->get_return_url($order));
            exit;
        }
    }

    /**
     * Thank-you text.
     */
    private function is_flow_complete($order) {
        $flow = get_post_meta($order->id, Oraqus_Tbk_Gateway::FLOW_META_KEY, true);
        Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s] - %4$s', __FUNCTION__, 'current flow', $order->id, print_r($flow, true))); 
        return in_array('ack', $flow);
    }

    /**
     * Thank-you text.
     */
    public function thankyou_text($order) {
        $options = get_option('woocommerce_' . Oraqus_Tbk_Gateway::PLUGIN_ID . '_settings');
        $my_account_page_id = get_option('woocommerce_myaccount_page_id');
        $my_account = $my_account_page_id ? get_permalink($my_account_page_id) : '';

        Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s]', __FUNCTION__, 'notification message', print_r($options['notification_message'], true)));
        Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s]', __FUNCTION__, 'my account permalink', print_r($my_account, true)));
        return sprintf('%1$s %2$s',
            $options['notification_message'],
            sprintf('[<a href="%1$s">%2$s</a>]', $my_account, __('My Account', 'oraqus-wc-transbank')));
    }

    /**
     * Register all of the hooks related to the public-facing functionality of the plugin.
     */
    private function define_public_hooks() {
        $plugin_public = new Oraqus_Tbk_Public($this->plugin_name, self::VERSION);
        $this->loader->register_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->register_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');
    }

    /**
     * Register all of the hooks related to the admin area functionality of the plugin.
     */
    private function define_admin_hooks() {
        $plugin_admin = new Oraqus_Tbk_Admin($this->plugin_name, self::VERSION);
        $transition_api_name = sprintf('woocommerce_api_%s_%s', self::PLUGIN_ID, self::TRANSITION_API_NAME);

        $this->loader->register_action('admin_enqueue_scripts',                               $plugin_admin, 'enqueue_styles');
        $this->loader->register_action('admin_enqueue_scripts',                               $plugin_admin, 'enqueue_scripts');
        $this->loader->register_action('admin_enqueue_scripts',                               $plugin_admin, 'enqueue_scripts');
        $this->loader->register_action($transition_api_name,                                  $this->client, 'transition_handler');
        $this->loader->register_action(sprintf('woocommerce_receipt_%s',  self::PLUGIN_ID),   $this, 'generate_receipt');
        $this->loader->register_action('woocommerce_order_details_after_order_table',         $this, 'show_payment_details');
        $this->loader->register_action('woocommerce_order_items_table',                       $this, 'show_rejection_note');
        $this->loader->register_action('woocommerce_thankyou_order_received_text',            $this, 'thankyou_text');
        $this->loader->register_action(sprintf('woocommerce_thankyou_%s',  self::PLUGIN_ID),  $this, 'order_received_process');
    }

    /**
     * Check if this gateway is enabled and available in the user's country.
     * @return bool
     */
    private function is_valid_for_use() {
        return in_array(get_woocommerce_currency(), apply_filters('oraqus_tbk_supported_currencies', array('CLP', 'USD')));
    }

    /**
     * Get the description of the payment type given.
     * @return string
     */
    private function get_type_description($code) {
        return (isset(self::$PAYMENT_TYPES[$code]) ? self::$PAYMENT_TYPES[$code] : $code);
    }

    /**
     * Get the description of the payment type given.
     * @return string
     */
    private function get_multiple_payments_description($code) {
        return (isset(self::$MULTIPLE_PAYMENT_TYPES[$code]) ? self::$MULTIPLE_PAYMENT_TYPES[$code] : $code);
    }

    /**
     * Save the administration options.
     */
    private function save_settings() {
        if (is_admin()) {
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
        }
    }

    /**
     * Turn these settings into variables we can use
     */
    private function extract_settings_to_vars() {
        foreach ($this->settings as $setting_key => $value) {
            $this->$setting_key = $value;
        }
    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the files that make up the plugin.
     */
    private function load_dependencies() {
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/abstract/abstract-oraqus-tbk-credentials.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/abstract/abstract-oraqus-tbk-environments.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-oraqus-tbk-loader.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-oraqus-tbk-i18n.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-oraqus-tbk-client.php';

        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-oraqus-tbk-admin.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-oraqus-tbk-public.php';
    }

    /**
     * Define the locale for this plugin for internationalization.
     */
    private function set_locale() {
        $plugin_i18n = new Oraqus_Tbk_I18n();
        $this->loader->register_action('plugins_loaded', $plugin_i18n, 'load_translations');
    }
}
