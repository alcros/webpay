<?php
/**
 * Oraqus_Tbk_Credentials
 *
 * @category   AbstractClass
 * @package    Oraqus_Tbk_Gateway/abstracts
 * @author     Oraqus
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link       https://oraqus.cl
 */

/**
 * Abstract class for transbank credentials.
 */
abstract class Oraqus_Tbk_Credentials {
    /**
     * @var      string $id The ID for these credentials.
     */
    protected $id;
    /**
     * @var      string $webpay_cert  The Webpay certificate to use to connect to Transbank.
     */
    protected $webpay_cert;

    /**
     * @var      string $private_key  The protected key needed to guarantee a secure communication with Transbank.
     */
    protected $private_key;

    /**
     * @var      string $public_cert  The protected certificate you created and provided to Transbank.
     */
    protected $public_cert;

    /**
     * @var      string $merchant_id  The Merchant Id provided by Transbank.
     */
    protected $merchant_id;

    /**
     * Factory method in charge of constructing the right set of credentials depending on the current environment
     * setting.
     */
    public static function create($settings) {
        if ('prd' === $settings['environment']) {
            return new Oraqus_Tbk_Production_Credentials($settings['webpay_cert'], $settings['private_key'],
                $settings['public_cert'], $settings['merchant_id']);
        }

        $credentials = require_once ORAQUS_TBK_PLUGIN_PATH . 'includes/test-credentials.php';
        if ('stg' === $settings['environment']) {
            return new Oraqus_Tbk_Staging_Credentials($credentials['stg']);
        }

        return new Oraqus_Tbk_Development_Credentials($credentials['dev']);
    }

    public function get_id() { return $this->id; }
    public function get_webpay_cert() { return $this->webpay_cert; }
    public function get_private_key() { return $this->private_key; }
    public function get_public_cert() { return $this->public_cert; }
    public function get_merchant_id() { return $this->merchant_id; }

    public function __toString() {
        return "{ 'credentials': " . "{" . "'id':'" . $this->id . 
            "', 'merchant_id':'" . $this->merchant_id . 
            "'}" . "}";
    }
}

/**
 * Credentials for staging.
 */
class Oraqus_Tbk_Staging_Credentials extends Oraqus_Tbk_Credentials {

    protected function __construct($creds) {
        $this->id = 'oraqus-wc-stg-credentials';
        $this->webpay_cert = $creds['webpay_cert'];
        $this->private_key = $creds['private_key'];
        $this->public_cert = $creds['public_cert'];
        $this->merchant_id = $creds['merchant_id'];
    }
}

/**
 * Credentials for development.
 */
class Oraqus_Tbk_Development_Credentials extends Oraqus_Tbk_Credentials {

    protected function __construct($creds) {
        $this->id = 'oraqus-wc-dev-credentials';
        $this->webpay_cert = $creds['webpay_cert'];
        $this->private_key = $creds['private_key'];
        $this->public_cert = $creds['public_cert'];
        $this->merchant_id = $creds['merchant_id'];
    }
}

/**
 * Credentials for production.
 */
class Oraqus_Tbk_Production_Credentials extends Oraqus_Tbk_Credentials {

    protected function __construct($wpay_cert, $key, $pub_cert, $mid) {
        $this->id = 'oraqus-wc-prd-credentials';
        $this->webpay_cert   = $wpay_cert;
        $this->private_key   = $key;
        $this->public_cert   = $pub_cert;
        $this->merchant_id   = $mid;
    }
}
