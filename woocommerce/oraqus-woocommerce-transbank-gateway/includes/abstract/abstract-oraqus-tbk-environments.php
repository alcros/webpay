<?php
/**
 * Oraqus_Tbk_Environment
 *
 * @category   AbstractClass
 * @package    Oraqus_Tbk_Gateway/abstracts
 * @author     Oraqus
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link       https://oraqus.cl
 */

/**
 * Environment.
 */
class Oraqus_TBK_Environment {
    const DEV = 'dev';
    const STG = 'stg';
    const PRD = 'prd';

    private static $options = array('dev' => "INTEGRACION", 'stg' => "CERTIFICACION", 'prd' => "PRODUCCION");

    public static function as_enum($description) {
        return array_search($description, self::$options);
    }

    public static function as_description($enum) {
        return self::$options[$enum];
    }
}

