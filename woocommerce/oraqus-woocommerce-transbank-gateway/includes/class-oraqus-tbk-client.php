<?php

/**
 * The client in charge of exchanging information with Transbank's service.
 *
 * Connects to the WebService via WS-Security to allow Transbank to make the charge on the credit card.
 *
 * @link       http://oraqus.cl
 * @since      1.0
 * @package    Oraqus_Tbk_Gateway
 * @subpackage Oraqus_Tbk_Gateway/includes
 * @author     Oraqus
 *
 */

class Oraqus_Tbk_Client {
    /**
     * @var      Configuration $config The libwebpay/Configuration to use.
     */
    protected $config;

    /**
     * @var      Webpay       $webpay    The Webpay object provided to access the service via a WebService endpoint.
     */
    protected $webpay;

    /**
     * @var      string       $receipt_url The URL to show at the end of the transaction.
     */
    protected $receipt_url;

    /**
     * @var      string       $base_url  The base URL to access API defined functionality.
     */
    private   $base_url;

    /**
     * Constructor
     */
    public function __construct($env, $credentials) {
        $this->load_dependencies();

        $this->config = new Configuration();
        $this->config->setEnvironment(Oraqus_TBK_Environment::as_description($env));
        $this->config->setCommerceCode($credentials->get_merchant_id());
        $this->config->setPrivateKey($credentials->get_private_key());
        $this->config->setPublicCert($credentials->get_public_cert());
        $this->config->setWebpayCert($credentials->get_webpay_cert());

        $this->base_url = sprintf('http://%1$s:%2$s/', $_SERVER['SERVER_NAME'], $_SERVER['SERVER_PORT']);
    }

    /**
     * Set the URL to show the final status.
     */
    public function set_receipt_url($receipt_url) {
        $this->receipt_url = $receipt_url;
    }

    /**
     * Make a request to Webpay.
     */
    public function invoke_service($order) {
        $this->webpay = new Webpay($this->config);
        $transition_handler_url = sprintf('%s?wc-api=%s_%s', $this->base_url, Oraqus_Tbk_Gateway::PLUGIN_ID, Oraqus_Tbk_Gateway::TRANSITION_API_NAME);
        $amount = (int) number_format($order->get_total(), 0, ',', '');
        $session_id = sprintf('%s:%s', $order->id, substr(wc_rand_hash(), 0, 7));
        update_post_meta($order->id, Oraqus_Tbk_Gateway::SESSION_ID_META_KEY, $session_id);

        $params = array(
            'amount' => $amount,
            'order_id' => $order->id,
            'session_id' => $session_id,
            'transition_url' => $transition_handler_url,
            'receipt_url' => $this->receipt_url
        );

        Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s', __FUNCTION__, 'init a transaction with the service'));
        $result = array();
        try {
            $result = $this->webpay->getNormalTransaction()->initTransaction( $amount, $order->id, $session_id, $transition_handler_url, $this->receipt_url);
            Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s] - %4$s', __FUNCTION__, 'setting transient', print_r($result->token, true), print_r($params, true)));
            set_transient($result->token, $params);
            Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s]', __FUNCTION__, 'storing session id', print_r($session_id, true)));
            update_post_meta($order->id, Oraqus_Tbk_Gateway::SESSION_ID_META_KEY, $session_id);
            $this->register_step($order->id, 'init');
        } catch (Exception $e) {
            $result['error'] = "Something failed when connecting to Webpay";
            $result['detail'] = $e->getMessage();
        } finally {
            Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s]', __FUNCTION__, 'init received', print_r($result, true)));
        }

        if (is_array($result) or empty($result->token) or empty($result->url)) {
            //log the error, the 'pretty' message to the user will be handled one layer up
            $result['detail'] = empty($result['detail']) ? 'no detail received' : $result['detail'];
            error_log(sprintf('%1$s: %2$s [%3$s]', __FUNCTION__,
                'Got an error or didn\'t get a token/url when initializing the transaction with the service',
                $result['error'] . ':' . $result['detail']));
            return $result;
        }

        require_once ORAQUS_TBK_PLUGIN_PATH . 'includes/templates/pay-for-order.php';
        exit;
    }

    /**
     * Transition page to handle the incoming token or a rejected/failed transaction.
     */
    public function transition_handler() {
        //Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s]', __FUNCTION__, 'server', print_r($_SERVER, true)));
        Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s', __FUNCTION__, 'handling transition'));

        if (!($this->valid($_POST))) {
            error_log(sprintf('%1$s: %2$s addr=%3$s referer=%4$s [%5$s]', __FUNCTION__, 'received an invalid post from',
                isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'unknown ip',
                isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'unknown referer',
                print_r($_POST, true)));
            exit;
        }

        $this->webpay = new Webpay($this->config);

        $token = $_POST["token_ws"];
        $result = array();
        Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s]', __FUNCTION__, 'retrieve the transaction result', print_r($token, true)));
        try {
            $result = $this->webpay->getNormalTransaction()->getTransactionResult($token);
        } catch (Exception $e) {
            $result['error'] = "Something failed when connecting to Webpay";
            $result['detail'] = $e->getMessage();
        } finally {
            Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s]', __FUNCTION__, 'transaction result', print_r($result, true)));
        }

        list($order_id, $destination, $meta_content) = $this->extract_from($result, $token);
        $order = wc_get_order($order_id);

        $this->register_step($order->id, 'get');
        if ($order->needs_payment() and !is_array($result)) {
            Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s', __FUNCTION__, 'confirm transaction'));
            $err = array();
            try {
                $this->webpay->getNormalTransaction()->acknowledgeTransaction($token);
                $this->register_step($order->id, 'ack');
            } catch (Exception $e) {
                $err['error'] = "Something failed when connecting to Webpay";
                $err['detail'] = $e->getMessage();
                error_log(sprintf('%1$s: %2$s [%3$s]', __FUNCTION__, 'confirmation process failed', print_r($err, true)));
            }
        }
        update_post_meta($order->id, Oraqus_Tbk_Gateway::PAYMENT_RESULT_META_KEY, $meta_content);

        $this->update_order_status($order, $result);

        require_once ORAQUS_TBK_PLUGIN_PATH . 'includes/templates/transition.php';
        exit;
    }

    /**
     * update the order status according to the result.
     */
    public function update_order_status($order, $result) {
        if (is_array($result)) {
            $order->update_status('failed', __('Couldn\'t make the payment', 'oraqus-wc-transbank'));
            error_log(sprintf('%1$s: %2$s [%3$s]', __FUNCTION__, 'An error was detected', print_r($result, true)));
            return;
        }

        if ($result->detailOutput->responseCode === 0) {
            Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s] - %4$s', __FUNCTION__, 'update order status', $order->id,'complete'));
            $order->payment_complete();
        } else {
            Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s] - %4$s', __FUNCTION__, 'update order status', $order->id, 'failed'));
            $order->update_status('failed', __('Payment failed or was declined', 'oraqus-wc-transbank'));
        }
    }

    /**
     * add the given step to the flow, a typical successful order has: init, get and ack.
     */
    private function register_step($order_id, $step) {
        $flow = get_post_meta($order_id, Oraqus_Tbk_Gateway::FLOW_META_KEY, true);
        $flow = $flow ? $flow: array(); //initialize if there was no meta
        array_push($flow, $step);
        //Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s:%4$s] - %5$s', __FUNCTION__, 'register step', $order_id, $step, print_r($flow, true)));
        Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s] - %4$s', __FUNCTION__, 'register step', $order_id, $step));
        update_post_meta($order_id, Oraqus_Tbk_Gateway::FLOW_META_KEY, $flow);
    }

    /**
     * Extract a tuple from the result and use the token to retrieve previously stored data if necessary
     * to fill information about a failed order.
     */
    private function extract_from($result, $token) {
        $oid = null;
        $dest = null;
        if (!is_array($result)) {
            $oid = $result->buyOrder;
            $dest = isset($result->urlRedirection) ? $result->urlRedirection : $this->receipt_url;
            $meta = $result;
        } else {
            //this code handles the case where a cert is no longer valid while in the middle of a payment
            $order_params = get_transient($token);
            $oid = '';
            $dest= '';
            $meta = new transactionResultOutput();
            $meta->cardDetail = new cardDetail();
            $meta->detailOutput = new wsTransactionDetailOutput();
            $meta->detailOutput->responseDescription = $result['error'];
            if (isset($order_params)) {
                $oid = $order_params['order_id'];
                $dest = $order_params['receipt_url'];
            } else {
                error_log(sprintf('%1$s: %2$s', __FUNCTION__, 'couldn\'t get order params from transient'));
            }
        }
        return array($oid, $dest, $meta);
    }

    /**
     * Validate the posted data.
     */
    private function valid($post_data) {
        if (empty($post_data) or !$this->is_whitelisted($_SERVER) or !(isset($post_data['token_ws']))) {
            return false;
        }
        return true;
    }

    /**
     * Check if the given param is whitelisted.
     */
    private function is_whitelisted($server) {
        $origin = isset($server['HTTP_ORIGIN']) ? $server['HTTP_ORIGIN'] : '';
        Oraqus_WC_Transbank::log('debug', sprintf('%1$s: %2$s [%3$s]', __FUNCTION__, 'origin', print_r($origin, true)));
        if (empty($origin) or !filter_var($origin, FILTER_VALIDATE_URL)) {
            //mozilla doesn't set it, chrome sets it as null
            return true;
        }

        $pattern = '/^https:\/\/([\w_-]+\.)*' . 'transbank.cl' . '$/';
        return preg_match($pattern, $origin) ? true : false;
    }

    /**
     * Load the required dependencies for this class.
     */
    private function load_dependencies() {
        require_once ORAQUS_TBK_PLUGIN_PATH . 'includes/libwebpay/webpay.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-oraqus-tbk-gateway.php';
    }
}
