<?php
/**
 * Activator
 *
 * @link       http://oraqus.cl
 * @since      1.0.0
 *
 * @package    Oraqus_Tbk_Gateway
 * @subpackage Oraqus_Tbk_Gateway/includes
 * @author     Oraqus <carlos.duque@mail.com>
 */
class Oraqus_Tbk_Activator {
    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate() {
    }
}
