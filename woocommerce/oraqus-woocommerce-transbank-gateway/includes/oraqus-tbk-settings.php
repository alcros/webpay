<?php
if ( ! defined('ABSPATH') ) {
    exit;
}
/**
 * Settings
 */
return apply_filters('woocommerce_' . Oraqus_Tbk_Gateway::PLUGIN_ID . '_settings',
    array(
        'enabled' => array(
            'title'   => __('Enable/Disable', 'oraqus-wc-transbank'),
            'type'    => 'checkbox',
            'label'   => __('Enable Oraqus Transbank Payment', 'oraqus-wc-transbank'),
            'default' => 'yes'
        ),
        'logging' => array(
            'title'   => __( 'Enable/Disable logging', 'oraqus-wc-transbank' ),
            'type'    => 'checkbox',
            'label'   => __( 'Enable application logging', 'oraqus-wc-transbank' ),
            'default' => 'yes'
        ),
        'title' => array(
            'title'       => __('Title', 'oraqus-wc-transbank'),
            'type'        => 'text',
            'description' => __('This controls the title for the payment method the customer sees during checkout.', 'oraqus-wc-transbank'),
            'default'     => __('Oraqus Transbank Payment', 'oraqus-wc-transbank'),
            'desc_tip'    => true,
        ),
        'notification_message' => array(
            'title'       => __('Notification Message', 'oraqus-wc-transbank'),
            'type'        => 'textarea',
            'description' => __('The notification message to be shown at the end of the transaction.', 'oraqus-wc-transbank'),
            'default'     => __('Your order has been received, please visit your account if you need more information', 'oraqus-wc-transbank'),
            'desc_tip'    => true,
        ),
        'environment' => array(
            'title'       => __('Transbank Active Environment', 'oraqus-wc-transbank'),
            'type'        => 'select',
            'description' => __('This setting specifies whether you will process simulated transactions for test or certification purposes, or real transactions using Transbanks production servers', 'oraqus-wc-transbank'),
            'default'     => 'dev',
            'desc_tip'    => true,
            'options'     => array(
                'dev'    => __('Integration', 'oraqus-wc-transbank'),
                'stg'    => __('Certification', 'oraqus-wc-transbank'),
                'prd'    => __('Production', 'oraqus-wc-transbank')
            ),
        ),
        'api_settings' => array(
            'title'       => __('API Production Settings', 'oraqus-wc-transbank'),
            'type'        => 'title',
            'description' => '',
        ),
        'merchant_id' => array(
            'title'       => __('Merchant Id', 'oraqus-wc-transbank'),
            'type'        => 'text',
            'description' => __('The code given to you by Transbank (production environment)', 'oraqus-wc-transbank'),
            'default'     => '111222333444',
            'desc_tip'    => true,
        ),
        'webpay_cert' => array(
            'title'       => __('Webpay Certificate', 'oraqus-wc-transbank'),
            'type'        => 'textarea',
            'description' => __('Webpay Certificate for real transactions (production environment)', 'oraqus-wc-transbank'),
            'default'     => __('[webpay cert here]', 'oraqus-wc-transbank'),
            'desc_tip'    => true,
        ),
        'private_key' => array(
            'title'       => __('Private Key', 'oraqus-wc-transbank'),
            'type'        => 'textarea',
            'description' => __('Webpay Private Key for real transactions (production environment)', 'oraqus-wc-transbank'),
            'default'     => __('[your key here]', 'oraqus-wc-transbank'),
            'desc_tip'    => true,
        ),
        'public_cert' => array(
            'title'       => __('Public Certificate', 'oraqus-wc-transbank'),
            'type'        => 'textarea',
            'description' => __('Public Certificate for real transactions (production environment)', 'oraqus-wc-transbank'),
            'default'     => __('[your cert here]', 'oraqus-wc-transbank'),
            'desc_tip'    => true,
        ),
    )
);
