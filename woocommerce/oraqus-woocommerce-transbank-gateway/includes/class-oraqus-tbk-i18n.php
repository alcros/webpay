<?php
/**
 * Fired during plugin activation
 *
 * @link       http://oraqus.cl
 *
 * @package    Oraqus_Tbk_Gateway
 * @subpackage Oraqus_Tbk_Gateway/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @package    Oraqus_Tbk_Gateway
 * @subpackage Oraqus_Tbk_Gateway/includes
 * @author     Oraqus <carlos.duque@mail.com>
 */
class Oraqus_Tbk_I18n {
    /**
     * Load the plugin text domain for translation.
     *
     * @since    1.0.0
     */
    public function load_translations() {
        load_plugin_textdomain('oraqus-wc-transbank', false, dirname(plugin_basename(__FILE__ ) ) . '/languages/' );
    }
}
