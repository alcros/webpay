<?php
/**
 * Fired during plugin deactivation
 *
 * @link       http://oraqus.cl
 * @since      1.0.0
 *
 * @package    Oraqus_Tbk_Gateway
 * @subpackage Oraqus_Tbk_Gateway/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Oraqus_Tbk_Gateway
 * @subpackage Oraqus_Tbk_Gateway/includes
 * @author     Oraqus <carlos.duque@mail.com>
 */
class Oraqus_Tbk_Deactivator {
    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function deactivate() {
    }
}
