#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Oraqus WooCommerce Transbank Gateway\n"
"POT-Creation-Date: 2017-05-10 23:03-0300\n"
"PO-Revision-Date: 2017-03-14 15:21-0400\n"
"Last-Translator: Carlos Duque <carlos.duque@mail.com>\n"
"Language-Team: Oraqus <carlos.duque@mail.com>\n"
"Language: es_CL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: .\n"

#: includes/class-oraqus-tbk-client.php:160
msgid "Couldn't make the payment"
msgstr ""

#: includes/class-oraqus-tbk-client.php:170
msgid "Payment failed or was declined"
msgstr ""

#: includes/class-oraqus-tbk-gateway.php:102
msgid "Payments through Transbank's Webpay system."
msgstr ""

#: includes/class-oraqus-tbk-gateway.php:155
msgid "Gateway disabled"
msgstr ""

#: includes/class-oraqus-tbk-gateway.php:157
msgid "Oraqus Transbank plugin does not support your store currency."
msgstr ""

#: includes/class-oraqus-tbk-gateway.php:170
msgid "Couldn't connect to <em>Webpay</em>"
msgstr ""

#: includes/class-oraqus-tbk-gateway.php:193
msgid "Couldn't connect to <em>Webpay</em>, please try again later."
msgstr ""

#: includes/class-oraqus-tbk-gateway.php:217
msgid "multiple payments type not defined"
msgstr ""

#: includes/class-oraqus-tbk-gateway.php:222
msgid "payment type not defined"
msgstr ""

#: includes/class-oraqus-tbk-gateway.php:248
#, php-format
msgid "%1$s <a href=\"%2$s\">My Account</a>"
msgstr ""

#: includes/oraqus-tbk-settings.php:11
msgid "Enable/Disable"
msgstr ""

#: includes/oraqus-tbk-settings.php:13
msgid "Enable Oraqus Transbank Payment"
msgstr ""

#: includes/oraqus-tbk-settings.php:17
msgid "Enable/Disable logging"
msgstr ""

#: includes/oraqus-tbk-settings.php:19
msgid "Enable application logging"
msgstr ""

#: includes/oraqus-tbk-settings.php:23
msgid "Title"
msgstr ""

#: includes/oraqus-tbk-settings.php:25
msgid ""
"This controls the title for the payment method the customer sees during "
"checkout."
msgstr ""

#: includes/oraqus-tbk-settings.php:26
msgid "Oraqus Transbank Payment"
msgstr ""

#: includes/oraqus-tbk-settings.php:30
msgid "Notification Message"
msgstr ""

#: includes/oraqus-tbk-settings.php:32
msgid "The notification message to be shown at the end of the transaction."
msgstr ""

#: includes/oraqus-tbk-settings.php:33
msgid ""
"Your order has been received, please visit your account if you need more "
"information"
msgstr ""

#: includes/oraqus-tbk-settings.php:37
msgid "Transbank Active Environment"
msgstr ""

#: includes/oraqus-tbk-settings.php:39
msgid ""
"This setting specifies whether you will process simulated transactions for "
"test or certification purposes, or real transactions using Transbanks "
"production servers"
msgstr ""

#: includes/oraqus-tbk-settings.php:43
msgid "Integration"
msgstr ""

#: includes/oraqus-tbk-settings.php:44
msgid "Certification"
msgstr ""

#: includes/oraqus-tbk-settings.php:45
msgid "Production"
msgstr ""

#: includes/oraqus-tbk-settings.php:49
msgid "API Production Settings"
msgstr ""

#: includes/oraqus-tbk-settings.php:54
msgid "Merchant Id"
msgstr ""

#: includes/oraqus-tbk-settings.php:56
msgid "The code given to you by Transbank (production environment)"
msgstr ""

#: includes/oraqus-tbk-settings.php:61
msgid "Webpay Certificate"
msgstr ""

#: includes/oraqus-tbk-settings.php:63
msgid "Webpay Certificate for real transactions (production environment)"
msgstr ""

#: includes/oraqus-tbk-settings.php:64
msgid "[webpay cert here]"
msgstr ""

#: includes/oraqus-tbk-settings.php:68
msgid "Private Key"
msgstr ""

#: includes/oraqus-tbk-settings.php:70
msgid "Webpay Private Key for real transactions (production environment)"
msgstr ""

#: includes/oraqus-tbk-settings.php:71
msgid "[your key here]"
msgstr ""

#: includes/oraqus-tbk-settings.php:75
msgid "Public Certificate"
msgstr ""

#: includes/oraqus-tbk-settings.php:77
msgid "Public Certificate for real transactions (production environment)"
msgstr ""

#: includes/oraqus-tbk-settings.php:78
msgid "[your cert here]"
msgstr ""

#: includes/templates/pay-for-order.php:11
msgid "Proceed with payment"
msgstr ""

#: includes/templates/pay-for-order.php:15
msgid ""
"Please be aware that this button will redirect you to an external site to "
"guarantee the security of your payment"
msgstr ""

#: includes/templates/payment-details.php:9
#: includes/templates/rejection.php:9
msgid "Payment Details"
msgstr ""

#: includes/templates/payment-details.php:16
msgid "Response Description:"
msgstr ""

#: includes/templates/payment-details.php:23
msgid "Authorization Code:"
msgstr ""

#: includes/templates/payment-details.php:30
msgid "Business Name:"
msgstr ""

#: includes/templates/payment-details.php:37
msgid "Amount:"
msgstr ""

#: includes/templates/payment-details.php:44
msgid "Type of Payments:"
msgstr ""

#: includes/templates/payment-details.php:51
msgid "Number of Payments:"
msgstr ""

#: includes/templates/payment-details.php:58
msgid "Transaction Date:"
msgstr ""

#: includes/templates/payment-details.php:65
msgid "Payment Type:"
msgstr ""

#: includes/templates/payment-details.php:72
msgid "Card Number:"
msgstr ""

#: includes/templates/payment-details.php:79
msgid "Card Expiration Date:"
msgstr ""

#: includes/templates/payment-details.php:86
msgid "Merchant Session Identifier:"
msgstr ""

#: includes/templates/rejection.php:12
msgid "Rejected Transaction"
msgstr ""

#: includes/templates/rejection.php:13
#, php-format
msgid "Purchase Order: %1$s"
msgstr ""

#: includes/templates/rejection.php:14
msgid "Possible rejection causes:"
msgstr ""

#: includes/templates/rejection.php:16
msgid "Wrong credit card information (date and/or verification code)"
msgstr ""

#: includes/templates/rejection.php:17
msgid "Insufficient funds"
msgstr ""

#: includes/templates/rejection.php:18
msgid "Invalid/Disabled credit card"
msgstr ""

#: oraqus-woocommerce-transbank-gateway.php:38
msgid "I can't do much when called direcly, I'm just a plugin man !"
msgstr ""

#: oraqus-woocommerce-transbank-gateway.php:130
#, php-format
msgid ""
"The minimum PHP version required for this plugin is %1$s. You are running "
"%2$s."
msgstr ""

#: oraqus-woocommerce-transbank-gateway.php:135
msgid "This plugin requires WooCommerce to be activated to work."
msgstr ""

#: oraqus-woocommerce-transbank-gateway.php:139
#, php-format
msgid ""
"The minimum WooCommerce version required for this plugin is %1$s. You are "
"running %2$s."
msgstr ""

#: oraqus-woocommerce-transbank-gateway.php:152
msgid "Settings"
msgstr ""
