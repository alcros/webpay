<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://oraqus.cl
 * @since             0.1
 * @package           Oraqus_Tbk_Gateway
 * @author            Oraqus
 *
 * Copyright(c) 2017 Oraqus.
 *
 * The name of Oraqus may not be used to endorse or promote products derived from this
 * software without specific prior written permission. THIS SOFTWARE IS PROVIDED ``AS IS'' AND
 * WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 * @wordpress-plugin
 * Plugin Name: Oraqus Transbank Gateway for WooCommerce
 * Version: 1.0
 * Plugin URI:  https://oraqus.cl/plugins
 * Description: Accept debit and credit card payments in your store using Transbank's Webpay Plus
 * Author:      Oraqus
 * Author URI:  http://oraqus.cl/
 * License:     EULA
 * License URI: https://www.gnu.org/licenses/eula.html
 * Requires at least: 4.7.2
 * Tested up to: 4.7.3
 *
 * Text Domain: oraqus-wc-transbank
 * Domain Path: /languages
 */
if(!function_exists('add_action')) {
    echo __('I can\'t do much when called direcly, I\'m just a plugin man !', 'oraqus-wc-transbank');
    die;
}

/**
 * Required minimums and constants
 */
define('ORAQUS_TBK_VERSION', '1.0');
define('ORAQUS_TBK_MIN_PHP_VER', '5.6');
define('ORAQUS_TBK_MIN_WC_VER', '2.6.13');
define('ORAQUS_TBK_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('ORAQUS_TBK_PLUGIN_DIR_NAME', basename(__DIR__));
if(! class_exists('Oraqus_WC_Transbank')):

    class Oraqus_WC_Transbank {

        /**
         * @var Singleton The reference the *Singleton* instance of this class
         */
        private static $instance;

        /**
         * @var Reference to logging class.
         */
        private static $logger;

        /**
         * @var Log option set for this plugin.
         */
        private static $log_setting;

        /**
         * Returns the *Singleton* instance of this class.
         *
         * @return Singleton The *Singleton* instance.
         */
        public static function get_instance() {
            if(null === self::$instance) {
                self::$instance = new self();
            }
            return self::$instance;
        }

        /**
         * logs messages to a file inside WC_LOG_DIR.
         */
        public static function log($level = 'debug', $message) {

            if (null === self::$logger) {
                self::$logger = new WC_Logger();
            }

            if (is_null(self::$log_setting)) {
                $options = get_option('woocommerce_' . Oraqus_Tbk_Gateway::PLUGIN_ID . '_settings');
                self::$log_setting = $options['logging'];
            }

            if ('yes' === self::$log_setting ) {
                self::$logger->add('oraqus-wc-transbank', sprintf('[%s] - %s', $level, $message));
            }
        }

        /**
         * Protected constructor to prevent creating a new instance of the
         * *Singleton* via the `new` operator from outside of this class.
         */
        protected function __construct() {
            add_action('plugins_loaded', array($this, 'init'));
        }

        /**
         * Init the plugin after plugins_loaded so environment variables are set.
         */
        public function init() {
            // Don't hook anything else in the plugin if we're in an incompatible environment
            if(self::check_environment()) {
                add_action('admin_notices', array($this, 'check_environment'));
                return;
            }

            // Init the gateway itself
            $this->init_gateways();

            add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'plugin_action_links'));
        }

        /**
         * Checks the environment for compatibility problems.  Returns a string with the first incompatibility
         * found or false if the environment has no problems.
         */
        static function check_environment() {
            if(version_compare(phpversion(), ORAQUS_TBK_MIN_PHP_VER, '<')) {
                $message = __('The minimum PHP version required for this plugin is %1$s. You are running %2$s.', 'woocommerce-gateway-stripe');
                return sprintf($message, ORAQUS_TBK_MIN_PHP_VER, phpversion());
            }

            if(! defined('WC_VERSION')) {
                return __('This plugin requires WooCommerce to be activated to work.', 'woocommerce-gateway-stripe');
            }

            if(version_compare(WC_VERSION, ORAQUS_TBK_MIN_WC_VER, '<')) {
                $message = __('The minimum WooCommerce version required for this plugin is %1$s. You are running %2$s.', 'woocommerce-gateway-stripe');
                return sprintf($message, ORAQUS_TBK_MIN_WC_VER, WC_VERSION);
            }

            return false;
        }

        /**
         * Adds plugin action links
         */
        public function plugin_action_links($links) {
            $section_slug = 'oraqus-wc-transbank';
            $plugin_links = array('<a href="' . admin_url('admin.php?page=wc-settings&tab=checkout&section='
                                  . $section_slug) . '">' . __('Settings', 'oraqus-wc-transbank') . '</a>',);
            return array_merge($plugin_links, $links);
        }

        /**
         * Initialize the gateway. Called very early - in the context of the plugins_loaded action
         */
        public function init_gateways() {
            if(! class_exists('WC_Payment_Gateway')) {
                return;
            }

            include_once(dirname(__FILE__) . '/includes/class-oraqus-tbk-gateway.php');
            load_plugin_textdomain('oraqus-wc-transbank', false, plugin_basename(dirname(__FILE__)) . '/languages');
            add_filter('woocommerce_payment_gateways', array($this, 'add_gateways'));
        }

        /**
         * Add the gateways to WooCommerce
         */
        public function add_gateways($methods) {
            $methods[] = 'Oraqus_Tbk_Gateway';
            return $methods;
        }

    }

    $GLOBALS['oraqus_tbk_transbank'] = Oraqus_WC_Transbank::get_instance();

endif;
