<?php
    echo "<h1>Simulator</h1>\n";
    echo '<p>Hello token ' . htmlspecialchars($_POST["token"]) . '</p>';
    $animal = array("gazzelle", "whale", "rat", "hawk", "horse", "aardvark", "kiwi");
    $adjective = array("feisty", "quick", "jumpy", "sneezy", "collaborative", "wiggly", "hungry");
    $codename = $adjective[rand(1, 6)] . "_" . $animal[rand(1, 6)];

    //$callbackUrl = "http://wp.cl:80/?wc-api=transition_handler_oraqustbk";
    $callbackUrl = "http://" . strstr($_SERVER["HTTP_HOST"], ':', true) . ":30080/?wc-api=oraqustbk_transition_handler";
?>

<h2>Gateway</h2>
<br />
<p>Your secret code is: <em><?php echo $codename; ?></em></p>
<p>I will post to url: <em><?php echo $callbackUrl; ?></em></p>
<br />
<form action="<?php echo $callbackUrl; ?>" method="post">
    <legend>Information</legend>
    <label for="cc">cc:</label>
    <input type="text" name="cc" id="cc" />
    <input type="text" name="token" id="token" value="<?php echo $_POST["token"] . ":". $codename; ?>" />
    <br />
    <input type="submit" />
</form>

<?php
