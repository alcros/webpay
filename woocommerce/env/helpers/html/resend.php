<?php

echo "[**]Print and Resend Service\n";
echo ' ** Hello ' . $_POST["name"] . " with token " . htmlspecialchars($_POST["token"]) . '!' . ', resending to ' . htmlspecialchars($_POST["url"]);

$animal = array("gazzelle", "whale", "rat", "hawk", "horse", "aardvark", "kiwi");
$adjective = array("feisty", "quick", "jumpy", "sneezy", "collaborative", "wiggly", "hungry");

$post_data['token'] = $adjective[rand(1, 6)] . "_" . $animal[rand(1, 6)];
$post_data['token'] = $adjective[rand(1, 6)] . "_" . $animal[rand(1, 6)];
$post_data['date'] = date('Y-m-d');

foreach ( $post_data as $key => $value) {
    $post_items[] = $key . '=' . $value;
}
$post_string = implode ('&', $post_items);
echo "\n ** params to post => " . $post_string . "\n";

$curl_connection = curl_init($_POST["url"]);
curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($curl_connection, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);

$result = curl_exec($curl_connection);
echo ">> printing the result\n";
echo "[--]\n";
echo "$result\n";
echo "[/-]\n";

echo ">> printing curl_getinfo: \n";
print_r(curl_getinfo($curl_connection));
echo curl_errno($curl_connection) . '-' . 
curl_error($curl_connection);
curl_close($curl_connection);

?>
